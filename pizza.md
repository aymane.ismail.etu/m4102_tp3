# Projet REST avec Jersey 
# Ces éléments sont inspirés par ce qui est fourni pour ingrédient !

La classe `ApiV1` sera le point d'entrée de notre application REST qui
permet de configurer le chemin de l'URI (`@ApplicationPath`) ainsi que
les paquetages Java qui contiennent les ressources.


## Développement d'une ressource Pizza

### API et représentation des données


| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                 |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------   |
| /pizzas             	   | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas (I2)                                   |
| /pizzas/{id}             | GET         | <-application/json<br><-application/xml                      |                 | une pizza ou 404                                   |
| /pizzas/{id}/name        | GET         | <-text/plain                                                 |                 | le nom de la pizza ou 404                               |
| /pizzas                  | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza (P1)      | Nouvelle pizza <br>409 si les ingredients exisitent |
| /pizzas/{id}             | DELETE      |                                                              |                 |                                                         |


Une pizza comporte un id, le nom de la pizza et des ingredients. Sa
représentation JSON (P2) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "Margherita",
      "ingredients" : ["tomate", "mozzarella"]
    }

Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente une pizza. Aussi on aura une
représentation JSON (P1) qui comporte un id, le nom de la pizza et des ingredient :

    { "name": "Margherita", "ingredients" : ["tomate", "mozzarella"]}
    
#### JavaBeans
JavaBean est un standard pour les objets Java permettant de les créer
et de les initialiser et de les manipuler facilement. Pour cela ces
objets doivent respecter un ensemble de conventions :

  - la classe est sérialisable
  - elle fournit au moins un constructeur vide
  - les attributs privés de la classe sont manipulables via des
    méthodes publiques **get**_Attribut_ et **set**_Attribut_

Les DTO et la classe Pizza décrits dans la suite sont des
JavaBeans.

#### Data Transfer Object (DTO)
JavaBean est un standard pour les objets Java permettant de les créer
et de les initialiser et de les manipuler facilement. Pour cela ces
objets doivent respecter un ensemble de conventions :

la classe est sérialisable
elle fournit au moins un constructeur vide
les attributs privés de la classe sont manipulables via des
méthodes publiques getAttribut et setAttribut


Les DTO et la classe Pizza décrits dans la suite sont des
JavaBeans.

#### Data Access Object (DAO)
Le DAO permet de faire le lien entre la représentation objet et le
contenu de la base de données.
Nous utiliserons la librairie JDBI qui permet
d'associer une interface à des requêtes SQL.
La classe BDDFactory qui vous est fournie permet un accès facilité
aux fonctionnalités de JDBI.

#### La représentation des données manipulées par la ressource
La classe Pizza est un JavaBean qui représente ce qu'est un
ingrédient. Elle porte des méthodes pour passer de cette
représentation aux DTO.
Cela permet de découpler l'implémentation de la ressource qui traite
les requêtes HTTP et la donnée manipulée.


    
