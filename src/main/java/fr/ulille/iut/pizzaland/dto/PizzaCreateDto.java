package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaCreateDto {
	private String name;
	private List<Ingredient> ingredients;

	public PizzaCreateDto() {
		this.ingredients = new ArrayList<>();
	}

	public PizzaCreateDto(String name) {
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String toString() {
		return "PizzaCreateDto [name=" + name + ", ingredients=" + ingredients + "]";
	}
}
