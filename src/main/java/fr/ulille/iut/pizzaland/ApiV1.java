package fr.ulille.iut.pizzaland;

import org.glassfish.jersey.server.ResourceConfig;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.ApplicationPath;

@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig {
	private static final Logger LOGGER = Logger.getLogger(ApiV1.class.getName());

	public ApiV1() {
		packages("fr.ulille.iut.pizzaland");
		String environment = System.getenv("PIZZAENV");

		if ( environment != null && environment.equals("withdb") ) {
			LOGGER.info("Loading with database");

			try {
				/* CHARGEMENT DES INGREDIENTS, PIZZAS ET COMMANDES 
				 * 
				 * Pour charger commandes et pizzas decommentez les lignes ci dessous
				 * 
				 * */
				FileReader reader = new FileReader( getClass().getClassLoader().getResource("ingredients.json").getFile() );
				//FileReader reader2 = new FileReader( getClass().getClassLoader().getResource("pizzas.json").getFile() );
				//FileReader reader3 = new FileReader( getClass().getClassLoader().getResource("commandes.json").getFile() );

				List<Ingredient> ingredients = JsonbBuilder.create().fromJson(reader, new ArrayList<Ingredient>(){}.getClass().getGenericSuperclass());
				//List<Pizza> pizzas = JsonbBuilder.create().fromJson(reader2, new ArrayList<Pizza>(){}.getClass().getGenericSuperclass());
				//List<Commande> commandes = JsonbBuilder.create().fromJson(reader3, new ArrayList<Commande>(){}.getClass().getGenericSuperclass());

				IngredientDao ingredientDao = BDDFactory.buildDao(IngredientDao.class);
				ingredientDao.dropTable();
				ingredientDao.createTable();
				for ( Ingredient ingredient: ingredients) {
					ingredientDao.insert(ingredient); 
				}
				
				/*   
                PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
                pizzaDao.dropAssociationTable();
                pizzaDao.dropPizzaTable();
                pizzaDao.createPizzaTable();
                pizzaDao.createAssociationTable();

                for ( Pizza p: pizzas) {
                	pizzaDao.insertWithIngredients(p); 
            }

                CommandeDao commandeDao = BDDFactory.buildDao(CommandeDao.class);
                commandeDao.dropAssociationTable();
                commandeDao.dropTable();
                commandeDao.createTable();
                commandeDao.createAssociationTable();

                for ( Commande c: commandes) {
                	commandeDao.insertionOfPizzas(c); 
            }
				 */


			} catch ( Exception ex ) {
				throw new IllegalStateException(ex);
			}
		} 
	}
}
