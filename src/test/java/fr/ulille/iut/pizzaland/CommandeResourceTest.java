package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */

public class CommandeResourceTest extends JerseyTest{
	private CommandeDao dao;
	//private static final Logger LOGGER = Logger.getLogger(CommandeResource.class.getName());

	@Override
	protected Application configure() {
		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(CommandeDao.class);
		dao.createAssociationTable();
		dao.createTable();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropAssociationTable();
		dao.dropTable();
	}

	@Test
	public void testGetEmptyList() {
		// La méthode target() permet de préparer une requête sur une URI.
		// La classe Response permet de traiter la réponse HTTP reçue.
		Response response = target("/commandes").request().get();
		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());	
		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<CommandeDto> commandes;
		commandes = response.readEntity(new GenericType<List<CommandeDto>>() {});	
		assertEquals(0, commandes.size());
	}

	@Test
	public void testGetExistingCommande() {
		Commande commande = new Commande();
		commande.setName("Aymane Ismail");
		dao.insertionOfPizzas(commande);
		Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());	
		Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
		assertEquals(commande, result);	
	}

	@Test
	public void testGetNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}

	@Test
	public void testCreateCommande() {

		Commande maCommande = new Commande();
		Ingredient monIngredient = new Ingredient();	
		Pizza maPizza = new Pizza();

		monIngredient.setName("Tomate");
		maPizza.setName("Margherita");

		List<Pizza> pizzas = new ArrayList<>();
		pizzas.add(maPizza);	

		maCommande.setName("Aymane Ismail");
		maCommande.setPizzas(pizzas);

		IngredientDto ingredientDto = Ingredient.toDto(monIngredient);
		ingredientDto.setName("Tomate");

		target("/ingredients").request().post(Entity.json(ingredientDto));      
		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(maPizza);
		target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		CommandeCreateDto commandeCreateDto = Commande.toCreateDto(maCommande);
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		CommandeDto returnedEntity = response.readEntity(CommandeDto.class);
		assertEquals(target("/commandes/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), commandeCreateDto.getName());
	}

	@Test
	public void testCreateSameCommande() {
		Commande commande = new Commande("Aymane Ismail");
		dao.insertionOfPizzas(commande);	
		CommandeCreateDto commandeCreateDto = Commande.toCreateDto(commande);
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));		
		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateCommandeWithoutName() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));
		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testDeleteExistingCommande() {
		Commande commande = new Commande();
		commande.setName("Aymane Ismail");
		dao.insertionOfPizzas(commande);
		Response response = target("/commandes/").path(commande.getId().toString()).request().delete();
		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());	
		Commande result = dao.findById(commande.getId());
		assertEquals(result, null);
	}

	@Test
	public void testDeleteNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetCommandeName() {
		Commande commande = new Commande();
		commande.setName("Aymane Ismail");
		dao.insertionOfPizzas(commande);
		Response response = target("commandes").path(commande.getId().toString()).path("name").request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertEquals("Aymane Ismail", response.readEntity(String.class));
	}

	@Test
	public void testGetNotExistingCommandeName() {
		Response response = target("commandes").path(UUID.randomUUID().toString()).path("name").request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateWithForm() {
		Form form = new Form();
		form.param("name", "Aymane Ismail");
		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("commandes").request().post(formEntity);
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Commande result = dao.findById(UUID.fromString(id));
		assertNotNull(result);
	}
}
