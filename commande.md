# Projet REST avec Jersey

La classe `ApiV1` sera le point d'entrée de notre application REST qui
permet de configurer le chemin de l'URI (`@ApplicationPath`) ainsi que
les paquetages Java qui contiennent les ressources.

# Ces éléments sont inspirés par ce qui est fourni pour ingrédient !

## Développement d'une ressource Commande

### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource *commandes*. Celle-ci devrait répondre aux URI suivantes :

| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------|
| /commandes               | GET         | <-application/json<br><-application/xml                      |                 | liste commandes (I2)                             |
| /commandes/{id}          | GET         | <-application/json<br><-application/xml                      |                 | commande 404                             |
| /commandes/{id}/name     | GET         | <-text/plain                                                 |                 | nom de la commande 404                         |
| /commandes               | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Commande (P1)   | nouvelle commande <br>409 si les pizzas existent |
| /commandes/{id}          | DELETE      |                                                              |      suprimmer commande           |                                                      |


Une commande comporte un id, un nom pour la commande et une ou plusieurs pizzas. Sa
représentation JSON :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "Aymane Ismail",
      "pizzas" : ["Margherita"]
    }

Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente une commande. Aussi on aura une
représentation JSON (P1) qui comporte un id, un nom pour la commande et une ou plusieurs pizzas :

    { "name": "Aymane Ismail", "pizzas" : ["Margherita"]}
    
#### JavaBeans
JavaBean est un standard pour les objets Java permettant de les créer
et de les initialiser et de les manipuler facilement. Pour cela ces
objets doivent respecter un ensemble de conventions :

  - la classe est sérialisable
  - elle fournit au moins un constructeur vide
  - les attributs privés de la classe sont manipulables via des
    méthodes publiques **get**_Attribut_ et **set**_Attribut_

Les DTO et la classe `Commande` décrits dans la suite sont des
JavaBeans.

#### Data Transfer Object (DTO)
JavaBean est un standard pour les objets Java permettant de les créer
et de les initialiser et de les manipuler facilement. Pour cela ces
objets doivent respecter un ensemble de conventions :

la classe est sérialisable
elle fournit au moins un constructeur vide
les attributs privés de la classe sont manipulables via des
méthodes publiques getAttribut et setAttribut


Les DTO et la classe COMMANDE décrits dans la suite sont des
JavaBeans.

#### Data Access Object (DAO)
Le DAO permet de faire le lien entre la représentation objet et le
contenu de la base de données.
Nous utiliserons la librairie JDBI qui permet
d'associer une interface à des requêtes SQL.
La classe BDDFactory qui vous est fournie permet un accès facilité
aux fonctionnalités de JDBI.

#### La représentation des données manipulées par la ressource
La classe Commande est un JavaBean qui représente ce qu'est un
ingrédient. Elle porte des méthodes pour passer de cette
représentation aux DTO.
Cela permet de découpler l'implémentation de la ressource qui traite
les requêtes HTTP et la donnée manipulée.
    
